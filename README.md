# botPortaDAEE

Este é um programa para controle de acesso através de um boot do telegram.

Para saber como criar um bot do telegram, acesse o tutorial o site abaixo

https://portal.vidadesilicio.com.br/controlando-seu-projeto-com-telegram-esp/

## Hardware
Diferentemente do tutorial citado acima que usa um ESP8266, neste foi utilizado um ESP32


![](PortaDAEE.png)

> Topologia

## Instruções para compilar
Para compilar, é necessário criar um arquivo chamado "secretes.h" que contenha:

```
#define BOTtoken "999999:AAAABBBBCCCCDDDDEEEEFFFFaaaabbbbcc"//Define o Token do *seu* BOT
#define EAP_ANONYMOUS_IDENTITY ""
#define EAP_IDENTITY "user@edu.provedor" //if connecting from another corporation, use identity@organisation.domain in Eduroam 
#define EAP_PASSWORD "password" //your Eduroam password
const char* ssid = "eduroam"; // Eduroam SSID
```

## Bibliotecas:

https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot

ArduinoJson <- Instale a partir do menu de ferramentas da IDE do arduino, mas não utilize as versões Beta.


https://arduinojson.org/?utm_source=meta&utm_medium=library.properties

Exemplos de conexões para a rede Eduroam
https://github.com/martinius96/ESP32-eduroam


## Regulamentação
##### 5.5.5.5
Locais que, por questões de segurança,necessitam manter as portas de saída de emergência permanentemente fechadas e trancadas, devem possuir dispositivo de liberação das portas, através de acionador manual de emergência que deverá atender os seguintes requisitos:
  - a) o acionador manual deverá estar localizado a no máximo 0,30 m da porta, permitindo o destravamento da porta no sentido da rota de fuga, a uma altura entre 0,90 m e 1,20 m do piso acabado;
  - b) o sistema de travamento e liberação da porta deverá ser elétrico/eletromagnético, com tensão máxima 30 Vcc;
  - c) o sistema deve permitir o destravamento da porta, mesmo com a falta de energia elétrica;
  - d) após a ativação do acionador manual, a folha da porta deve permanecer destravada até que o acionador seja rearmado manualmente;
  - e) o sistema de alarme de incêndio, quando existente, deve liberar as portas de emergência quando acionado;
  - f) a porta de emergência no sentido da rota de fuga e o acionador manual devem ser iluminados por sistema de iluminação de emergência;
